"use client"
import { useSearchParams } from 'next/navigation'
import { useRef, useEffect } from 'react'

type Props = {
    title: string,
    onClose: () => void,
    onOk: () => void,
    children: React.ReactNode,
    showDialog: boolean
}

export default function Dialog({ title, onClose, onOk, showDialog, children }: Props) {

    const dialogRef = useRef<null | HTMLDialogElement>(null)
    
    useEffect(() => {
        if (showDialog) {
            dialogRef.current?.showModal()
        } else {
            dialogRef.current?.close()
        }
    }, [showDialog])

    const closeDialog = () => {
        dialogRef.current?.close()
        onClose()
    }

    const clickOk = () => {
        onOk()
        closeDialog()
    }

    const dialog: JSX.Element | null = showDialog === true
        ? (
            <dialog ref={dialogRef} className="fixed  left-50 -translate-x-50 -translate-y-50 z-10  rounded-xl backdrop:bg-gray-800/50">
                <div className="w-[500px] max-w-fullbg-gray-200 flex flex-col">
                    <div className="flex flex-row justify-between mb-4 pt-2 px-5 bg-gray-400 border-b">
                        <h1 className="text-2xl">{title}</h1>
                        <button
                            onClick={closeDialog}
                            className="mb-2 py-1 px-2 cursor-pointer rounded border-none w-8 h-8 font-bold bg-black-600 text-white"
                        >x</button>
                    </div>
                    <div className="px-3 pb-1">
                        {children}
                        <div className="flex flex-row justify-end mt-6">
                            <button
                                onClick={closeDialog}
                                className="bg-blue-500 py-1 px-6 mx-2 rounded border-none"
                            >
                                Cancelar
                            </button>
                            <button
                                onClick={clickOk}
                                className="bg-blue-500 py-1 px-6  rounded border-none"
                            >
                                Aceptar
                            </button>
                        </div>
                    </div>
                </div>
            </dialog>
        ) : null


    return dialog
}