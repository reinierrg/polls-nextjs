"use client";
import { useState, useEffect} from "react";
import axios, { AxiosError } from "axios";
import { useRouter } from "next/navigation";
import { useSession } from "next-auth/react";
import { IPoll, IOption } from "@/models/type";

function Polls() {
  const [error, setError] = useState("");
  const [polls, setPolls] = useState([]);
  const [options, setOptions] = useState([]);
  const [idSelectedOption, setIdSelectedOption] = useState<String>("");
  const [pollIdSelected, setPollIdSelected] = useState<String>("");
  const [notApplyPoll, setNotApplyPoll] = useState(true);
  const router = useRouter();

  const { data: session, status } = useSession();

  useEffect(() => {
    const pollslist = handlePollsList
    pollslist();
  },[]);

  const handlePollsList = async () => {
    try {
      const initialPolls = await axios.get("/api/poll");
      if (initialPolls?.data?.success){
        setPolls(initialPolls?.data?.polls);
      }
      
    } catch (error) {
      if (error instanceof AxiosError) {
        const errorMessage = error.response?.data.message;
        setError(errorMessage);
      }
    }
  };

  const handlePollOptions = async (id: String) => {
    
    try{
      const idPoll:String  = id;
      const idUser = session?.user?.id;
      
      const optionsToPoll = await axios.get('/api/option', {
        params: {
          idPoll,
          idUser
        }}
      );

      console.log(optionsToPoll);
      setPollIdSelected(idPoll);
      if (optionsToPoll?.data?.success) {
        setOptions(optionsToPoll?.data?.options);
        setNotApplyPoll(optionsToPoll.data.done);
      }
     
    } catch(error ){
      if (error instanceof AxiosError) {
        const errorMessage = error.response?.data.message;
        setError(errorMessage);
      }
    }
  }

  const handlerSelectOption = async () => {
    try {
  
      if (session?.user) {
        const idUser: String = session?.user?.id;
        const idOption = idSelectedOption;

        const selectedOption = await axios.post("/api/option/selected", {
          idUser,
          idOption
        });
        const {data} = selectedOption;

        console.log(data);

        if (data.success && data.options.length > 0) {
          setNotApplyPoll(data.done);
          setOptions(data.options);
        }
      }
    } catch(error) {
      if (error instanceof AxiosError) {
        const errorMessage = error.response?.data.message;
        setError(errorMessage || error.message);
      }
    }
  };

  const listComponentPoll = () => {

    return (<div className="flex flex-col">
         <table className="text-left text-sm font-light">
           <thead className="font-medium ">
             <tr>
               <th scope="col" className="px-6 py-4">Encuestas</th>
             </tr>
           </thead>
           <tbody>
            {
               polls.map((el, idx) => {
                 return (<tr key={`poll-tr${idx}`} className="border-b">
                   <td className="whitespace-nowrap px-4 py-3">
                   <div onClick={() => handlePollOptions((el as IPoll)._id)} className="flex justify-between">
                     <div>
                       {(el as IPoll)._id === pollIdSelected && 
                       <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"  stroke="currentColor" className="w-6 h-6">
                           <path  d="M9 12h3.75M9 15h3.75M9 18h3.75m3 .75H18a2.25 2.25 0 002.25-2.25V6.108c0-1.135-.845-2.098-1.976-2.192a48.424 48.424 0 00-1.123-.08m-5.801 0c-.065.21-.1.433-.1.664 0 .414.336.75.75.75h4.5a.75.75 0 00.75-.75 2.25 2.25 0 00-.1-.664m-5.8 0A2.251 2.251 0 0113.5 2.25H15c1.012 0 1.867.668 2.15 1.586m-5.8 0c-.376.023-.75.05-1.124.08C9.095 4.01 8.25 4.973 8.25 6.108V8.25m0 0H4.875c-.621 0-1.125.504-1.125 1.125v11.25c0 .621.504 1.125 1.125 1.125h9.75c.621 0 1.125-.504 1.125-1.125V9.375c0-.621-.504-1.125-1.125-1.125H8.25zM6.75 12h.008v.008H6.75V12zm0 3h.008v.008H6.75V15zm0 3h.008v.008H6.75V18z" />
                         </svg>
                       }
                     </div>
                     <h2>{(el as IPoll).ask}</h2>
                   </div>
                   </td>
                 </tr>)
               })
           }
           </tbody>
         </table>
       </div>)
  }
  
  const listComponentOption = () => {
    return notApplyPoll ? (
      <div key={`option-component`}  className="flex flex-col">
      <table className="min-w-full text-left text-sm font-light">
        <thead className="font-medium">
          <tr>
            <th scope="col" className="px-6 py-4">Respuestas</th>
          </tr>
        </thead>
        <tbody>{
          options.map((el, idx) => {
            return (<tr key={`option-tr${idx}`} className="border-b">
              <td key={`option-td${idx}`} className="whitespace-nowrap px-6 py-2">
                  <div className="flex justify-between items-center">
                    <label key={`option-label${idx}`} className="flex rounded-md px-3 py-2 my-3">
                      <i key={`i-input${idx}`} className="pl-2">{(el as IOption).answer}</i>
                    </label>
                    <span>{`${(el as IOption).porciento || 0}`}</span>
                  </div>
                </td>
              </tr>)
            })
          }
        </tbody>
      </table>
      </div>
    ) : (
      <div key={`option-component`}  className="flex flex-col">
        <table className="min-w-full text-left text-sm font-light">
          <thead className="font-medium">
            <tr>
              <th scope="col" className="px-6 py-4">Respuestas</th>
            </tr>
          </thead>
          <tbody>{
            options.map((el, idx) => {
              return (<tr key={`option-tr${idx}`} className="border-b">
                <td key={`option-td${idx}`} className="whitespace-nowrap px-6 py-2">
                  <label key={`option-label${idx}`} className="flex rounded-md px-3 py-2 my-3 cursor-pointer ">
                    <input key={`option-input${idx}`}  type="radio" name="Option" onClick={() => setIdSelectedOption((el as IOption)._id)}/>
                    <i key={`i-input${idx}`} className="pl-4">{(el as IOption).answer}</i>
                    </label>
                  </td>
                </tr>)
              })
            }
          </tbody>
        </table>
        {options.length > 0  && <button className="flex bg-blue-500 text-white px-4 py-2 block w-full rounded-xl mt-10" onClick={handlerSelectOption}>
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" className="w-6 h-6 mr-3">
            <path  d="M6 12L3.269 3.126A59.768 59.768 0 0121.485 12 59.77 59.77 0 013.27 20.876L5.999 12zm0 0h7.5" />
          </svg>
            Enviar
          </button>}
        </div>
    )
  }

  return (
    <>
     {/*<div className=".w-screen.h-screen.p-12.flex.justify-center.items-center.bg-gradient-to-r from-pink-400 to-purple-600" >
      <div className="w-full md:w-6/12 lg:w-5/12">
        <div className="relative">
         <input type="radio" name="user" id="user1" className="hidden peer" />
         <label for="user1" className="flex gap-4 p-4 roundel-xl bg-white bp-white bg-opacity-90 backdrop-blur-2x1 shadow-x1 hover:bg-opacity-75 peer-check:bg-purpule-900">
         </label>
        </div>
      </div> 
      </div>*/}
   <div className="grid grid-flow-col justify-stretch">
    <div className="px-3 py-10 ">
      {listComponentPoll()}
    </div>
    <div className="px-3 py-10">
      {listComponentOption()}
    </div>
  </div>
    </>
   
  );
}

export default Polls;
