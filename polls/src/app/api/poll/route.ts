import { connectDB } from "@/libs/mongodb";
import {Poll, Option} from "@/models/schema";
import { NextResponse, NextRequest } from "next/server";
import mongoose from "mongoose";

export async function POST(request: NextRequest) {
  try {
    await connectDB();

    const { ask, options } = await request.json();

    const poll = new Poll({
      ask,
      options: options
    });

    const savedPoll = await poll.save();
    let array_option = Array();

    if (savedPoll && options.length > 0) {
      options.forEach((element: String) => {
        const option = {
          answer: element,
          poll: savedPoll._id
        };

        array_option.push(option);
      });
  
      Option.insertMany(array_option);
    }

    return NextResponse.json(
      {
        savedPoll,
        success: true
      },
      { status: 200 }
    );
  } catch (error) {
    if (error instanceof mongoose.Error.ValidationError) {
      return NextResponse.json(
        {
          message: error.message,
          success: false
        },
        {
          status: 400,
        }
      );
    }
    return NextResponse.error();
  }
}

export async function GET() {
  try {
    await connectDB();
    const polls = await Poll.find({});

    return NextResponse.json(
      {
        polls,
        success: true
      },
      { status: 200 }
    );
  } catch (error) {
    if (error instanceof mongoose.Error.ValidationError) {
      return NextResponse.json(
        {
          message: error.message,
          success: false
        },
        {
          status: 400,
        }
      );
    }
    return NextResponse.error();
  }
}


export async function DELETE(req: NextRequest) {
  try {
    await connectDB();
    const { searchParams } = new URL(req.url);
    let id = searchParams.get("id");
   
    const deleteOpton = await Option.deleteMany({
      poll: {
        $eq: id,
      }
    })
    
    const pollDelete = await Poll.deleteOne({
      _id: {
        $eq: id,
      }
    })
    
    return NextResponse.json(
      {
        poll: pollDelete,
        success: true
      },
      { status: 200 }
    );
  } catch (error) {
    if (error instanceof mongoose.Error.ValidationError) {
      return NextResponse.json(
        {
          message: error.message,
          success: false
        },
        {
          status: 400,
        }
      );
    }
    return NextResponse.error();
  }
}
