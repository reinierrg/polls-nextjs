import { connectDB } from "@/libs/mongodb";
import { Option, User } from "@/models/schema";
import { NextResponse, NextRequest } from "next/server";
import {optionsUpdateStatisticsByPoll} from "@/utils/utilsApi"

export async function POST(request: Request) {
  try {
    await connectDB();

    const { idUser, idOption } = await request.json();
 
    const optionSelected = await Option.findById(idOption);
    const idpoll = optionSelected.poll;

    const optionCurrent = await Option.findOne({
      poll: {
        $eq: idpoll,
      },
      selected: {
        $elemMatch:{$eq: idUser}
      }
    }).limit(1);

    if (!optionCurrent?.id){
      optionSelected.selected.push(idUser);
      await optionSelected.save();

      let options = await optionsUpdateStatisticsByPoll(idpoll);

      return NextResponse.json({success: true , options, done: true});
    }

    if(optionSelected?._id != optionCurrent?.id) {
      const selectedCurrent = optionCurrent.selected;
      optionCurrent.selected = selectedCurrent.filter((user: String) => user != idUser);
      await optionCurrent.save();

      optionSelected.selected.push(idUser);
      await optionSelected.save();
    }

    let options = await optionsUpdateStatisticsByPoll(idpoll);
 
    return NextResponse.json({success: true , options, done: true});
  } catch (err) {
  
    return NextResponse.json({ message: err, success: false });
  }
}


        