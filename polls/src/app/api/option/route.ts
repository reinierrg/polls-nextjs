import { connectDB } from "@/libs/mongodb";
import { Option } from "@/models/schema";
import { NextResponse, NextRequest } from "next/server";
import mongoose from "mongoose";
import {optionsUpdateStatisticsByPoll, optionsByPoll, optionsCleanByPoll} from "@/utils/utilsApi"

export async function POST(request: NextRequest) {
  try {
    await connectDB();

    const { idPoll, newAnswer } = await request.json();

    const option = new Option({
      answer: newAnswer,
      poll: idPoll
    });

    await option.save();

    await optionsCleanByPoll(idPoll);

    return NextResponse.json(
      {
        option,
        success: true,
      },
      { status: 200 }
    );
  } catch (error) {
    if (error instanceof mongoose.Error.ValidationError) {
      return NextResponse.json(
        {
          message: error.message,
          success: false,
        },
        {
          status: 400,
        }
      );
    }
    return NextResponse.error();
  }
}


/**
 * funcion GET que me devuelve la lista de opciones que pertenecen a una encuestra al pasarle el id de la encuestra
 * @param req 
 * @returns 
 */
export async function GET(req: NextRequest) {
  try {
    await connectDB();
    const { searchParams } = new URL(req.url);
    let idPoll = searchParams.get("idPoll");
    let idUser = searchParams.get("idUser");
    let done = false;

    const optionfinded = await Option.findOne({
      poll: {
        $eq: idPoll,
      },
      selected: {
        $elemMatch:{$eq: idUser}
      }
    }).limit(1);

    let options = [];

    if (optionfinded?.id) {
      done = true;
      options = await optionsUpdateStatisticsByPoll(idPoll);
    } else {
      done = false;
      options = await optionsByPoll(idPoll);
    }

    //console.log(options, done);

    return NextResponse.json(
      {
        options,
        success: true,
        done,
        
      },
      { status: 200 }
    );
  } catch (error) {
    if (error instanceof mongoose.Error.ValidationError) {
      return NextResponse.json(
        {
          message: error.message,
        },
        {
          status: 400,
        }
      );
    }
    return NextResponse.error();
  }
}

export async function DELETE(req: NextRequest) {
  try {
    await connectDB();
    const { searchParams } = new URL(req.url);
    let id = searchParams.get("id");

    const listOption = await Option.deleteOne({
      _id: {
        $eq: id,
      }
    })

    return NextResponse.json(
      {
        option: listOption
      },
      { status: 200 }
    );
  } catch (error) {
    if (error instanceof mongoose.Error.ValidationError) {
      return NextResponse.json(
        {
          message: error.message,
        },
        {
          status: 400,
        }
      );
    }
    return NextResponse.error();
  }
}
        