"use client";
import { useState, useEffect, SyntheticEvent } from 'react';
import axios, { AxiosError } from 'axios';
import { useRouter } from 'next/navigation';
import { useSession } from 'next-auth/react';
import Dialog from "@/components/Dialog";
import { IOption, IPoll, ISession, IUser } from "@/models/type";
import {ItemPoll, ItemOption} from "@/components";
import { DefaultUser } from "next-auth";

const Admin = () => {

  const { data: session , status } = useSession({
      required: true,
  });

  const [error, setError] = useState<String>("");
  const [polls, setPolls] = useState([]);
  const [options, setOptions] = useState([]);
  const [newAnswer, setNewAnswer] = useState("");
  const [newAsk, setNewAsk] = useState("");
  const [pollIdSelected, setPollIdSelected] = useState<String>("");
  const [showDialogConfirmOptionDelete, setShowDialogConfirmOptionDelete] = useState(false);
  const [showDialogConfirmPollDelete, setShowDialogConfirmPollDelete] = useState(false);
  const [idSelectedDeleteOption, setIdSelectedDeleteOption] = useState<String>("");
  const [idSelectedDeletePoll, setIdSelectedDeletePoll] = useState<String>("");
  const router = useRouter();

  useEffect(() => {
    const pollslist = handlePollsList
    pollslist();
  },[]);

  const handlePollsList = async () => {
    try {
      const initialPolls = await axios.get("/api/poll");
      console.log(initialPolls);
      setPolls(initialPolls?.data?.polls);
    } catch (error: any) {
      if (error instanceof AxiosError) {
        let errorMessage = error.response?.data.message;
        setError(errorMessage);
      }
    }
  };

  const onHandlerDeletePoll = async () => {
    try{
      const idPoll:String  = idSelectedDeletePoll;
      const optionsToPoll = await axios.delete('/api/poll', {
        params: {
          id: idPoll,
        }}
      );

      if (optionsToPoll?.data?.poll?.acknowledged) {
        handlePollsList();
        setPollIdSelected("");
        handlePollOptions("");
      }
    
    } catch(error :any){
      if (error instanceof AxiosError) {
        const errorMessage = error.response?.data.message;
        setError(errorMessage);
      }
    }
  }

  const handlePollOptions = async (id: String) => {
    
    try{
      const idPoll: String  = id;
      const idUser = session?.user?.id;
      let optionsToPoll = {data:{options:[]}};

      if (id != "") {
        optionsToPoll = await axios.get('/api/option', {
          params: {
            idPoll,
            idUser
          }}
        );
      }

      setPollIdSelected(idPoll);
      setOptions(optionsToPoll?.data?.options);
      
    } catch(error: any){
      if (error instanceof AxiosError) {
        const errorMessage = error.response?.data.message;
        setError(errorMessage);
      }
    }
  }

  const handleAddOption = async () => {

    if(pollIdSelected &&  newAnswer){
     
      try {
        const optionsToPoll = await axios.post('/api/option', {
          idPoll: pollIdSelected,
          newAnswer
        });

        console.log(pollIdSelected, newAnswer, optionsToPoll);

        if (optionsToPoll?.data?.option?._id) {
          handlePollOptions(pollIdSelected);
        }

        setNewAnswer("");

      } catch(error: any){
        if (error instanceof AxiosError) {
          const errorMessage = error.response?.data.message;
          setError(errorMessage);
        }
      }
      
    }
  }

  const handleAddPoll = async () => {
    
    if(newAsk){
      try {
        const optionsToPoll = await axios.post('/api/poll', {
          ask: newAsk,
          options: [],
        });

        setNewAsk("");
        handlePollsList();

      } catch(error: any){
        if (error instanceof AxiosError) {
          const errorMessage = error.response?.data.message;
          setError(errorMessage);
        }
      }
      
    }
  }

  const handleDeleteOption = async (id: String) => {
    setIdSelectedDeleteOption(id);
    setShowDialogConfirmOptionDelete(true);
  }

  
  const handleDeletePoll = async (id: String) => {
    setIdSelectedDeletePoll(id);
    setShowDialogConfirmPollDelete(true);
  }

  const listComponentPoll = () => {

   return (<div className="flex flex-col">
      
        <table className="text-left text-sm font-light">
          <thead className="font-medium ">
            <tr>
              <th scope="col" className="px-6 py-4">Encuestas</th>
            </tr>
          </thead>
          <tbody>{
              polls.map((el, idx) => {
                return (<tr className="border-b" key={`poll-tr${idx}`}>
                  <td className="whitespace-nowrap px-4 py-3">
                    <ItemPoll 
                      onClick={() => handlePollOptions((el as IPoll)._id)}
                      onDelete={() => handleDeletePoll((el as IPoll)._id)}
                      selected={(el as IPoll)._id === pollIdSelected}
                      text={(el as IPoll).ask} 
                    />
                  </td>
                </tr>)
              })
          }
          </tbody>
        </table>
      </div>)
       
  }

  const listComponentOption = () => {
    return (
      <div className="flex flex-col">
        <table className="min-w-full text-left text-sm font-light">
          <thead className="font-medium">
            <tr>
              <th scope="col" className="px-6 py-4">Respuestas</th>
            </tr>
          </thead>
          <tbody>{
            options.map((el, idx) => {
              return (<tr key={`option-tr${idx}`} className="border-b">
                <td className="whitespace-nowrap px-6 py-4">
                  <ItemOption 
                    text={(el as IOption).answer} 
                    onDelete={() => handleDeleteOption((el as IOption)._id)} 
                    onClick={() => {}}/>
                  </td>
                </tr>)
              })
            }
          </tbody>
        </table>
        </div>
    );
  }

  
  const onHandlerDialogPoll = () => {
    setIdSelectedDeletePoll("");
    setShowDialogConfirmPollDelete(false);
  }

  const onHandlerDialogOption = () => {
    setIdSelectedDeleteOption("");
    setShowDialogConfirmOptionDelete(false);
  }

  const onHandlerDeleteOption = async () => {
     try{
        const idOption: String = idSelectedDeleteOption;
        const optionDelete = await axios.delete("/api/option", {
          params: {
            id: idOption,
          }}
        );

        if (optionDelete.data?.option?.acknowledged) {
          handlePollOptions(pollIdSelected);
        }

      } catch(error: any){
        if (error instanceof AxiosError) {
          const errorMessage = error.response?.data.message;
          setError(errorMessage);
        }
      }
  }

  const onKeyDownAsk = async (event: {keyCode: Number}) => {
    if (event.keyCode === 13) {
      await handleAddPoll();
    }
  }

  const onKeyDownAnswer = async (event: {keyCode: Number}) => {
    if (event.keyCode === 13) {
      await handleAddOption();
    }
  }

  return (session &&
    <>
      <div className="grid grid-flow-col justify-stretch">
        <div className="px-3 py-10 ">
          <>
            <input className="rounded-r rounded-l" value={newAsk} type="text" style={{color: "black"}} onChange={(e) => setNewAsk(e.target.value)} onKeyDown={(e) => onKeyDownAsk(e) }></input>
            <button className="flex bg-blue-500 text-white px-4 py-2 block w-full mt-4 rounded-xl" disabled={!newAsk}  onClick={() => handleAddPoll()}>
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" className="w-6 h-6 mr-4">
                <path d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5M16.5 12L12 16.5m0 0L7.5 12m4.5 4.5V3" />
              </svg>
              agregar
            </button>
          </>
          {listComponentPoll()}
        </div>
        <div className="px-3 py-10">
          <input className="rounded-l rounded-r" value={newAnswer} type="text" style={{color: "black"}} onChange={(e) => setNewAnswer(e.target.value)} onKeyDown={(e) => onKeyDownAnswer(e)}></input>
          <button className="flex bg-blue-500 text-white px-4 py-2 block w-full mt-4 rounded-xl" disabled={!newAnswer}  onClick={() => handleAddOption()}>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" className="w-6 h-6 mr-4">
              <path d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5M16.5 12L12 16.5m0 0L7.5 12m4.5 4.5V3" />
            </svg>
            agregar
          </button>
          {listComponentOption()}
        </div>
      </div>

      <Dialog title="Eliminar" onClose={onHandlerDialogOption} onOk={onHandlerDeleteOption} showDialog={showDialogConfirmOptionDelete} >
        <div>Desea eliminar la respuesta de la encuesta</div>
      </Dialog>

      <Dialog title="Eliminar" onClose={onHandlerDialogPoll} onOk={onHandlerDeletePoll} showDialog={showDialogConfirmPollDelete} >
        <div>Desea eliminar la encuestas</div>
      </Dialog>
    </>
   
  );
}

/*
export async function getServerSideProps() {
  try {
    const initialPolls = await axios.get("/api/poll");
    console.log(initialPolls);
   
    return {
      props: { polls: initialPolls?.data?.polls },
    }
  } catch (e) {
    console.error(e)
    return {
      props: { polls: false },
    }
  }
}*/

export default Admin;
