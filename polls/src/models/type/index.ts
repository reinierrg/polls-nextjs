import IPoll from "./poll";
import IOption from "./option";
import ISession from "./session";
import IUser from "./user";
import IError from "./error";

export type {
    IPoll,
    IOption,
    ISession,
    IUser,
    IError
} ;
