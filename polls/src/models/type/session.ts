import { DefaultUser } from "next-auth";
interface ISession {
    id: String,
    user: DefaultUser
}

export default ISession;