interface IOption {
    _id: String,
    answer: String,
    selected: [],
    porciento: Number,
}

export default IOption;