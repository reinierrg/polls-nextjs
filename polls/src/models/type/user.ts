interface IUser {
    _id: String,
    name: String,
    email: String,
    image: String
}

export default IUser;
 