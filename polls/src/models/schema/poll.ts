import { Schema, model, models } from "mongoose";

const PollSchema = new Schema(
  {
    ask: {
      type: String,
      unique: true,
      required: [true, "La pregunta es requrida"],
    },
    close: {
      type: Boolean,
      default: false
    },
  },
  {
    timestamps: true,
  }
);

const Poll = models.Poll || model("Poll", PollSchema);
export default Poll;
