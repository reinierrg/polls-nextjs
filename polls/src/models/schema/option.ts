import  mongoose, { Schema, model, models } from "mongoose";

const OptionSchema = new Schema(
  {
    answer: {
      type: String,
      required: [true, "La respuesta es requerida"],
    },
    selected: {
      type: Array,
      default:[]
    },
    poll: { 
      type: mongoose.Types.ObjectId,
      required: [true, "campo requerido"]
    },
    porciento: { 
      type: Number,
    }
  },
  {
    timestamps: true,
  }
);

const Option = models.Option || model("Option", OptionSchema);
export default Option;
