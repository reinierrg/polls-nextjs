import {Option} from "@/models/schema";

async function totalUsersApplyPoll(idPoll: String | null) {

    let total = 0;
    const options = await Option.find({
        poll: {
          $eq: idPoll,
        },
    });

    options.forEach(option => {
        total = total + option.selected.length
      }
    );
    
    return total
}

async function optionsUpdateStatisticsByPoll (idPoll: String | null) {

    let total = await totalUsersApplyPoll(idPoll);
    const options = await Option.find({
        poll: {
          $eq: idPoll,
        },
    });
     
    options.forEach(option => {
        let total_option = option.selected.length;
        let porciento =total_option*100/total;
        option.porciento = Number(porciento.toFixed(2));
    });

    return options;
}


async function optionsCleanByPoll (idPoll: String | null) {
    try {
        const options = await Option.find({
            poll: {
              $eq: idPoll,
            },
        });
         
        options.forEach(option => {
            option.selected = [];
            option.save();
        });

        return true;
    } catch(e) {
       return false;
    }
}


async function optionsByPoll (idPoll: String | null) {

    const options = await Option.find({
        poll: {
          $eq: idPoll,
        },
    });

    return options;
}



export {
    totalUsersApplyPoll,
    optionsUpdateStatisticsByPoll,
    optionsCleanByPoll,
    optionsByPoll
}