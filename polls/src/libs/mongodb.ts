import mongoose from "mongoose";

const { MONGO_SRV, DB_NAME } = process.env;

const MONGODB_URI = `${MONGO_SRV}/${DB_NAME}`;

if (!MONGODB_URI) {
  throw new Error("MONGODB_URI must be defined");
}

export const connectDB = async () => {
  try {
    const { connection } = await mongoose.connect(MONGODB_URI);
    if (connection.readyState === 1) {
      console.log("MongoDB Connected");
      return Promise.resolve(true);
    }
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
};
