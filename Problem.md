# Aplicacion de Encuestras

## Descripción del problema
Tu tarea es diseñar e implementar una solución para gestionar y ejecutar encuestas (usa como referencia visual las encuestas en Twitter o Telegram). El objetivo principal de esta tarea es evaluar sus habilidades de backend. Cualquier entregable inicial se evaluará como una ventaja.

## Páginas de aplicación:
- Páginas de inicio de sesión/registro:
    Usuario/contraseña estándar para autenticarse en el sistema.
    Para registrarse, solicite toda la información en un solo formulario y acceda al sistema con los usuarios autenticados.
    validado:
    - Los nombres de usuario deben ser únicos.
    - Establecer reglas para la configuración de contraseñas seguras.
    - Los nuevos usuarios tendrán el rol de Usuario.
- Perfil de Usuario (nombre de usuario, nombre completo, contraseña, avatar): página para configurar todos los elementos del perfil asociados a un usuario.
- Encuestas: Página donde cada usuario puede ver todas las encuestas del sistema. Tendrá dos formas de ver las encuestas (cambiando entre vistas):
    - Una vista de lista donde todas las encuestas se enumeran en un componente similar a una tabla. Si hace clic en una encuesta, aparecerá una barra lateral desde la derecha, que le mostrará los detalles de la encuesta, tendrá la oportunidad de votar y ver los resultados.
    - Una vista en mosaico donde cada encuesta está representada por un componente de tarjeta. Toda la votación/visualización de resultados se realizará dentro de la tarjeta. En esta vista, no se mostrará ninguna barra lateral.
    - Los usuarios podrán votar solo una vez por encuesta a menos que la encuesta sea editada. En ese caso, se le concederá nuevamente el permiso para votar.
- Página de administración de encuestas: donde los usuarios avanzados o administradores pueden ver la lista de encuestas, crear nuevas, editar o eliminar las existentes:
    - Encuestas tipo respuesta única.
    - Cada usuario puede votar sólo una vez por cada encuesta.
    - Se puede cerrar una encuesta, en ese caso ningún usuario podrá votar en la encuesta y solo se mostrarán los resultados.
    - El detalle de las encuestas se verá en una barra lateral que aparecerá en la parte derecha de la pantalla.
    - Cada vez que se modifica una encuesta existente, algún tipo de ayuda visual alertará a los usuarios en la página de encuestas que
      se cambió la encuesta (es decir: color de fondo diferente, algún ícono en la esquina de la tarjeta).
- Página de Gestión de Usuarios: página donde los usuarios con rol de Administrador pueden rastrear a los usuarios registrados en el sistema:
    - Los usuarios se pueden ver en una estructura tipo tabla.
    - Editar detalles. Los detalles del usuario se verán en una barra lateral de forma similar a las encuestas.
    - Roles de usuario:
        - Usuario: puede interactuar con encuestas ya creadas, ver los resultados y realizar su propio voto.
        - Usuario avanzado: igual que Usuarios. También puede crear nuevas encuestas, actualizar y eliminar las existentes.
        - Administrador: los mismos privilegios que los usuarios avanzados. También puede administrar usuarios (crear, eliminar, cambiar
          rol, contraseña).

## Acceso a la información
Los usuarios registrados tendrán acceso a las secciones del sistema en función del rol que se les haya otorgado.

## Requerimientos generales
Su propuesta debe utilizar las siguientes técnicas de desarrollo:
    - Correcto diseño y uso de rutas en la implementación de su API.
    - Dividir la solución en dos proyectos: Back-End y Front-End.
    - Implementar autenticación/autorización usando JWT.
    - Breve documentación sobre el proyecto y la arquitectura del proyecto (como archivos .md).

## Otros requerimientos
- Aplique manejo de errores y validación de datos para evitar fallas cuando se ingresan datos no válidos.
- Utilice un sistema de control de código fuente, como GitHub/Gitlab con más de 5 confirmaciones en el historial del proyecto.
- Aplicación de mejores prácticas de codificación.

## Requisitos opcionales
- Utilice un diseño responsivo: Bootstrap, MDL, CSS Grids u otro método de su elección.
- Uso de patrones de diseño para garantizar un rápido mantenimiento y escalabilidad del código.
- Para la aplicación web:
    - Interfaz de usuario atractiva, compatible con todos los navegadores web antiguos y modernos.
        - Uso de un marco o biblioteca CSS como
        Emoción [preferido] (https://emotion.sh/docs/introduction), Tailwindcss (https://tailwindcss.com/) o
        componentes de estilo (https://styled-components.com/)
        - Uso de Material UI (https://mui.com/) u otra biblioteca de componentes de su elección.
    - Buena usabilidad (UI fácil de usar)

## Requisitos de Software
- Lenguaje de programación: JavaScript, Typescript
- Marcos/Bibliotecas:
     - Back-end: NestJS o NodeJS con ExpressJS, Apollo GraphQL (opcional).
     - Front-end: ReactJS o NextJS.
- Base de datos: MongoDB

## Entregables:
- El código fuente en un sistema de cambios de versión (GitLab, Github)
    - ¡No envíe los paquetes NPM, YARN! No son necesarios y ocupan demasiado espacio en el disco.
- La documentación del proyecto.

## Bonificaciones
- Uso de funciones de HTML 5 como Geolocalización, Local Storage, SVG, Canvas, etc.
- Todo lo que no esté descrito en la tarea es una ventaja si tiene algún uso práctico.